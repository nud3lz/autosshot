autosshot
=====

## Intro

We all love the automatization of repetitive task. That's why this script was createad.
Usually to get an valid list for peepingtom or Eyewitness one have to perform a nmap scan,
look through the output and do some grep-foo to get the results in a file. 
This script automates that, it looks for the tools, start an nmap scan, filters the most 
common ports (80, 443, 8080, 8443) and calls peepingtom or Eyewitness to do their job.

## Setup

No setup should be needed except the installation of 

* Eyewitness:  https://github.com/ChrisTruncer/EyeWitness

or

* peepingtom:  https://bitbucket.org/LaNMaSteR53/peepingtom/

and (s)locate and running updatedb before.

## Usage

./autsshot.sh {eew|ppt} ip or ip range 

### Example

./autsshot.sh eew 10.10.1.0/24

Open a Browser of choice and maneuver to the location where this script is stored

file:///home/USER/assessment/sshots/

there eyewitness stores it's report

peepingtom reports are located in its dir, so one has to go there

file:///home/USER/peepingtom/


## Issues

* Peepingtom have to be run inside his installation directory 

*	No sanity check for IP's or IP ranges 


## ToDo's

* Improve sanity checks for Input

* move peepingtoms report to script home dir

*	introduce flags for input parameters 

* nmap scan with mapping and identification for http

* perhaps migrate to phyton or ruby
