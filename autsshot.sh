#!/bin/bash

########################################################
#	This script is preparing a list of IPs from a Nmap	 #
#	scan range and open ports											 		   #
# for EyeWitness or peepingtom 												 #
# author: nud3lz.tsing@gmail.com											 #
#	This script is licensed unter the WTFPL							 #
########################################################


# because of nmap root can run this script
if [[ $EUID -ne 0 ]]; then
   echo "This script must be run as root" 1>&2
   exit 1
fi
# ...


# check input parameters

if [ "$#" -eq 0 ];then
	echo "No arguments supplied!"
	SCRIPT_NAME=$(basename "$0")
	echo "please call $SCRIPT_NAME ppt or eew and ip or ip range "
	echo " try something like $SCRIPT_NAME eew 10.10.10.0/24"
	exit 1; 
fi

TOOL=$1;
IP=$2;


if [ -z "$TOOL" ] || [ -z "$IP" ];then
	echo "Given argument is empty..."
	exit 1;
fi

## Tests that didn't work as expected 
#if [[  ! "$TOOL" == ppt  ||  ! "$TOOL" == eew ]];then
#	echo "1fst parameter ist ppt, peepingtom or eew, EyeWitness"
#	exit 3;
#fi
# because the above aint work ffs!
#if [[ ! "$TOOL" == ppt ]];then 
#	echo "";	
#	elif [[ ! "$TOOL" == eew ]];then
#				echo "expected ppt or eew got $TOOL"
#				exit;
#fi

# rudimentary check if valid ip ... 300.300.999.999 still works
#if [[ ! $IP =~ ^[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}$ ]]; then
#if [[ ! "$IP" =~ ^(([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\.){3}([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])(\/([1-2]\d|3[0-2]|\d)) ]]; then
#	echo "Not an valid IP address"
#  exit 3;
#fi



DATE=`date +"%m-%d-%Y-%H%M%S"`
PORT_T="T:21-25,53,80,110-113,135-139,143,256,389,443,445,554,636,993-995,1723,3306,3389,5900,8080,8443,31339"
PORT_U="U:53,67-69,123,135-139,161,162,445,500,514,520,631,1434,1900,4500,37452,49152"
NMAP_OUTPUT="Nmap_grep_output-$DATE"
NMAP_PAR="-A -sS -sU -p $PORT_T,$PORT_U -oG $NMAP_OUTPUT $IP"


# check necessery binaries
NMAP=`whereis nmap | awk '{print $2}'`
if [ -z "$NMAP" ];then
				echo "WOW! could not find Nmap please install and put it in your path"
				exit;
fi

LOCATE=`whereis locate | awk  '{print $2}'`
if [ -z $LOCATE ];then
				echo "could not find Locate ... try to install (s)locate and execute updatedb!"
				echo "giving up :("
	exit 1;
fi				

# setting python2.7 env for peepingtom and eyewitness
PYT27=`whereis python2.7 |awk '{print $2}'`
if [ -z "$PYT27" ];then
	echo "Hmm, well thats too bad both tools require python 2.7 "
	exit 1;
fi


# checking for teh tools
EYEW=""
PPT=""

case $TOOL in 
				eew)
				EYEW=`whereis EyeWitness.py | awk '{print $2}'`
					if [ -z "$EYEW" ];then
						echo "whereis could not find in std path eyewitness..."
						echo "tryin to locate ..."
						EYEW=`locate EyeWitness.py`
						if [ -z $EYEW ];then
							echo "Could not locate Eyewitness..."
							echo "exiting..."
							exit;
						fi
					fi
					;;
				ppt)
				PPT=`whereis peepingtom.py |awk '{print $2}'`
				if [ -z $PPT ]; then 
						 echo "whereis could not find in std path peepingtom"	
						 echo "trying locate"
						 PPT=`locate peepingtom.py`
						 if [ -z $PPT ];then
										 echo "Could not locate Peepingtom... Lucky him i guess"
										 echo "exiting..."
										 exit;
							fi
			fi
								;;
esac

#locating current directory and creating one 
#for nmap log plus files from eew 
PWD=`pwd`
DIR="nmap-scan-$DATE"
mkdir -v $DIR;
cd $DIR;

# scan-foo

$NMAP $NMAP_PAR

cat $NMAP_OUTPUT |while read LINE;
do
				if echo $LINE | grep -m1 -e "80/open";
				then
								echo $LINE |awk '{print $2}'|sed 's/^/http:\/\//' >> ppt.lst
				fi
				if echo $LINE | grep -m1 -e "8080/open";
				then
								IP=`echo $LINE |awk '{print $2}'|sed 's/^/http:\/\//'`
								echo "$IP:8080"	>> ppt.lst
				fi
				if echo $LINE | grep -m1 -e "443/open";
				then
								IP=`echo $LINE |awk '{print $2}'|sed 's/^/https:\/\//'`
								echo "$IP:443" >> ppt.lst
				fi

				if echo $LINE | grep -m1 -e "8443/open";
				then
								IP=`echo $LINE |awk '{print $2}'|sed 's/^/https:\/\//'`
								echo "$IP:8443"	>> ppt.lst
				fi
done				

# lets screenshot all the things

if [ ! -z $EYEW ];then
				$PYT27 $EYEW -t 15 -f $PWD/ppt.lst -d $PWD/REPORT-$DATE
elif [ ! -z $PPT ];then
				# moo 
				OLD_PWD=$PWD
				PPT_DIR=`echo $PPT |sed 's/peepingtom.py//g'`
				cd $PPT_DIR
				$PYT27 $PPT -l $OLD_PWD/ppt.lst 
else
				echo "ehhm something ducked up! exiting..."
				exit 1;
fi

